const container = document.querySelector(".container");

fetch("https://feeds.behold.so/s7ocHbc5CX3LT9YnsZ45")
  .then((data) => data.json())
  .then((photos) => {
    photos.forEach(
      ({
        id, // The post ID
        mediaUrl, // The image source
        permalink, // URL of the Instagram post
        caption, // Post caption
        mediaType, // 'IMAGE', 'VIDEO', or 'CAROUSEL_ALBUM'
        thumbnailUrl, // Only returned for video posts
        timestamp, // Post publish date,
        children, // An array of CAROUSEL_ALBUM children. Each with id, mediaUrl, and mediaType
      }) => {
        // Step 3. Make magic happen...
        const pattern = "On a sorti la mini";
        let captionValue = caption;

        if (!captionValue.search(pattern)) {
          console.log(captionValue);

          // Create all DOM elements
          const item = document.createElement("a");
          const instaImg = document.createElement("img");
          const instaText = document.createElement("h2");

          // Add css style to new elements
          item.classList.add("item");
          instaImg.classList.add("instaImg");
          instaText.classList.add("instaText");

          // Fill new elements attributes
          item.setAttribute("href", permalink);
          item.setAttribute("target", "_blank");
          instaImg.setAttribute("src", mediaUrl);
          instaImg.setAttribute("alt", "Instagram post");

          // Add value to new elements
          instaText.innerHTML = caption;

          // Add all insta post to the dedicated container
          container.appendChild(item);
          item.appendChild(instaImg);
          item.appendChild(instaText);
        }
      }
    );
  });
