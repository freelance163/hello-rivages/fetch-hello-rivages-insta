// Ressource : https://ariasthompson.com/adding-instagram-feed-website-without-plugin/

var request = new XMLHttpRequest();
request.open('GET', 'https://api.instagram.com/v1/users/self/media/recent/?access_token=IGQVJVWjNoRHUwRWhXVmxjdjdIZAER2MWFRanhSLXNrc21lQVhkZAE5MMEs5b1pWc19qRF9EUVVGbnBENzJKSFZA4S3hYU1FkRlhBUld3Y1hYWEZAMejR6ZAmRBMWtZAQlJFcURma0djZAlRR&count=4', true);

request.onload = function(container) {
  if (request.status >= 200 && request.status < 400) {
    // Success!
    var data = JSON.parse(request.responseText);

    for (var i=0;i < data.data.length;i++) {
      // Get container element and fetch images URLs
      var container = document.getElementById('insta-feed');
      var imgURL = data.data[i].images.standard_resolution.url;
      console.log(imgURL);

      // Create image container
      var div = document.createElement('div');
      div.setAttribute('class','instapic');
      container.appendChild(div);

      // Add image to image conatiner
      var img = document.createElement('img');
      img.setAttribute('src',imgURL)
      div.appendChild(img);
    }

    console.log(data);
  } else {
  }
};

request.onerror = function() {
  // There was a connection error of some sort
};

request.send();